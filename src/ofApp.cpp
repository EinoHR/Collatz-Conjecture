#include "ofApp.h"

int currentNum;
int numIterator = 5;
int numRandom;
int i;
int currentY;
float hueIterator;
ofPath grandpath;
ofPath path;
ofEasyCam cam;

//--------------------------------------------------------------
void ofApp::setup(){
    ofSetFullscreen(true);
    currentNum = numIterator;
    ofBackground(0,0,0);
    grandpath.setFilled(false);
    grandpath.setStrokeWidth(2);
    grandpath.setStrokeColor(ofColor::fromHex(0xaccacf));
    path.setFilled(false);
    path.setStrokeWidth(2);
    
    cam.removeInteraction(ofEasyCam::TRANSFORM_ROTATE, OF_MOUSE_BUTTON_LEFT);
    cam.addInteraction(ofEasyCam::TRANSFORM_TRANSLATE_XY,OF_MOUSE_BUTTON_LEFT);
    cam.addInteraction(ofEasyCam::TRANSFORM_SCALE, OF_MOUSE_BUTTON_RIGHT);
    
    cam.enableOrtho();
    cam.setNearClip(-1000000);
    cam.setFarClip(1000000);
    cam.setVFlip(true);
    
    cam.setPosition(ofGetScreenWidth()/2, ofGetScreenHeight()/2, -20);
    
    gui.setup();
    gui.setWidthElements(350);
    gui.add(randomToggle.setup("Use random numbers instead of iterating", false));
    gui.add(randomMaxValue.setup("Maximum random number", 100, 5,9999999));
    gui.add(randomMinValue.setup("Minimum random number", 5, 5,9999999));

}

//--------------------------------------------------------------
void ofApp::update(){
    
    if (currentNum == 1){
        if (randomToggle == false){
            numIterator += 1;
            currentNum = numIterator;
        }
        else {
            numRandom = rand()%(randomMaxValue-randomMinValue + 1) + randomMinValue;
            currentNum = numRandom;
        }
        
        i = 0;
        path.moveTo(ofVec3f(0, ofGetScreenHeight(), numIterator));
        path.close();
        grandpath.append(path);
        path = ofPath();
        path.setFilled(false);
        path.setStrokeWidth(2);
        
        hueIterator += 17;
        if (hueIterator == 255){
            hueIterator = 0;
        }
    }
    if (currentNum % 2 == 0){
        currentNum = currentNum/2;
    }
    else {
        currentNum = currentNum*3+1;
    }
    
//    cout << currentNum;
//    cout << "\n";

    currentY = ofGetScreenHeight() - currentNum;
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    cam.begin();
    path.setStrokeColor(ofColor::fromHsb(hueIterator, 256, 256));
    path.lineTo(ofVec3f(i, currentY, numIterator));
    path.draw();
    grandpath.draw();
    cam.end();
    
    if (randomToggle){
        ofDrawBitmapString("Current starting number: "+to_string(numRandom), 0, ofGetScreenHeight());
    }
    else {
        ofDrawBitmapString("Current starting number: "+to_string(numIterator), 0, ofGetScreenHeight());
    }
    
    ofDrawBitmapString("Current number: "+to_string(currentNum), 0, ofGetScreenHeight()-10  );
    gui.draw();
    
    i += 10;
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
